package main

import (
	"io"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", helloWorldHandler)
	http.ListenAndServe(":8080", nil)
}
func helloWorldHandler(w http.ResponseWriter, r *http.Request) {
	name := os.Getenv("APPS_NAME")
	version := os.Getenv("VERSION")

	io.WriteString(w, "Please Welcome To The Jungle\nApps Name : "+name+"\nVersion : "+version)
}
